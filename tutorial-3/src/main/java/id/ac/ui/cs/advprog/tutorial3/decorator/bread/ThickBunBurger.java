package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

public class ThickBunBurger extends id.ac.ui.cs.advprog.tutorial3.decorator.Food {
    public ThickBunBurger() {
        setDescription("Thick Bun Burger");
    }

    @Override
    public double cost() {
        return 2.5;
    }
}
