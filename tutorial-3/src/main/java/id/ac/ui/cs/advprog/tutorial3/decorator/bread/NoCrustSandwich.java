package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

public class NoCrustSandwich extends id.ac.ui.cs.advprog.tutorial3.decorator.Food {
    public NoCrustSandwich() {
        setDescription("No Crust Sandwich");
    }

    @Override
    public double cost() {
        return 2.0;
    }
}
