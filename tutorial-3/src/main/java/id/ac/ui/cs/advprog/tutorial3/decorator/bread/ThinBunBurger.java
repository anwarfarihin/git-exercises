package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

public class ThinBunBurger extends id.ac.ui.cs.advprog.tutorial3.decorator.Food {
    public ThinBunBurger() {
        setDescription("Thin Bun Burger");
    }

    @Override
    public double cost() {
        return 1.5;
    }
}
