package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends FillingDecorator {
    Food food;
    public TomatoSauce(Food food) {
        this.food = food;
    }

    public String getDescription() {
        return food.getDescription() + ", Tomato Sauce";
    }


    public double cost() {
        return food.cost() + 0.2;
    }
}
