package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class BeefMeat extends FillingDecorator {
    Food food;

    public BeefMeat(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", Beef Meat";
    }

    @Override
    public double cost() {
        return food.cost() + 6.0;
    }
}
