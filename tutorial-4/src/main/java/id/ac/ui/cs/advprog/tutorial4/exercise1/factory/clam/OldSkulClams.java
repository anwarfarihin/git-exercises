package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class OldSkulClams implements Clams {

    public String toString() {
        return "Old Clams delicious for elder people";
    }
}
