package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;


public class NewYorkPizzaIngredientFactoryTest {

    private NewYorkPizzaIngredientFactory nyPizzaIngredientFactory;

    @Before
    public void setUp() {
        nyPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {

        ThinCrustDough thinCrustDough = new ThinCrustDough();
        assertEquals(thinCrustDough.getClass(), nyPizzaIngredientFactory.createDough().getClass());
        assertEquals(thinCrustDough.toString(), nyPizzaIngredientFactory.createDough().toString());

    }

    @Test
    public void testCreateCheese() {

        ReggianoCheese reggianoCheese = new ReggianoCheese();
        assertEquals(reggianoCheese.getClass(), nyPizzaIngredientFactory.createCheese().getClass());
        assertEquals(reggianoCheese.toString(), nyPizzaIngredientFactory.createCheese().toString());

    }

    @Test
    public void testCreateSauce() {

        MarinaraSauce marinaraSauce = new MarinaraSauce();
        assertEquals(marinaraSauce.getClass(), nyPizzaIngredientFactory.createSauce().getClass());
        assertEquals(marinaraSauce.toString(), nyPizzaIngredientFactory.createSauce().toString());

    }

    @Test
    public void testCreateVeggie() {

        Veggies[] veggies = {new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
        Veggies[] nyPizzaVeggies = nyPizzaIngredientFactory.createVeggies();
        IntStream.range(0, veggies.length).forEach(i -> assertEquals(veggies[i].toString(),
                nyPizzaVeggies[i].toString()));

    }

    @Test
    public void testCreateClam() {

        FreshClams freshClams = new FreshClams();
        assertEquals(freshClams.getClass(), nyPizzaIngredientFactory.createClam().getClass());
        assertEquals(freshClams.toString(), nyPizzaIngredientFactory.createClam().toString());
    }
}
