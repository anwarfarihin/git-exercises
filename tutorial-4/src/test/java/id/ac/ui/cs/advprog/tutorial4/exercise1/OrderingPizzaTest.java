package id.ac.ui.cs.advprog.tutorial4.exercise1;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import java.security.PublicKey;

import static org.junit.Assert.assertEquals;
public class OrderingPizzaTest {
    private NewYorkPizzaStore nyStore;
    private DepokPizzaStore dStore;

    @Before
    public void setUp() throws Exception {
        nyStore = new NewYorkPizzaStore();
        dStore = new DepokPizzaStore();
    }

    @Test
    public void testOrderCheesePizza() {
        Pizza nyPizza = nyStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", nyPizza.getName());
        Pizza dPizza = dStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", dPizza.getName());
    }

    @Test
    public void testOrderClamPizza() {
        Pizza nyPizza = nyStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", nyPizza.getName());
        Pizza dPizza = dStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", dPizza.getName());
    }

    @Test
    public void testOrderVeggiePizza() {
        Pizza nyPizza = nyStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", nyPizza.getName());
        Pizza dPizza = dStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", dPizza.getName());
    }
}
