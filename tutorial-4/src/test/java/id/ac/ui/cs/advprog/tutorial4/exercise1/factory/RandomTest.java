package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RandomTest {
    private PlumTomatoSauce plumTomatoSauce;
    private ThickCrustDough thickCrustDough;
    private FrozenClams frozenClams;

    @Before
    public void setUp() {
        plumTomatoSauce = new PlumTomatoSauce();
        thickCrustDough = new ThickCrustDough();
        frozenClams = new FrozenClams();
    }

    @Test
    public void testVeggiesName() {

        assertEquals(plumTomatoSauce.toString(), "Tomato sauce with plum tomatoes");
        assertEquals(thickCrustDough.toString(), "ThickCrust style extra thick crust dough");
        assertEquals(frozenClams.toString(), "Frozen Clams from Chesapeake Bay");

    }


}
