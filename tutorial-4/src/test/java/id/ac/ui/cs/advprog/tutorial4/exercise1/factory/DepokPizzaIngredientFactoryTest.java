package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.BunCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.OldSkulClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.MediumCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.KetchupSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.CassavaLeaf;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;


public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory depokPizzaIngredientFactory;

    @Before
    public void setUp() {

        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {

        MediumCrustDough mediumCrustDough = new MediumCrustDough();
        assertEquals(mediumCrustDough.getClass(),
                depokPizzaIngredientFactory.createDough().getClass());
        assertEquals(mediumCrustDough.toString(),
                depokPizzaIngredientFactory.createDough().toString());

    }

    @Test
    public void testCreateCheese() {

        BunCheese bunCheese = new BunCheese();
        assertEquals(bunCheese.getClass(),
                depokPizzaIngredientFactory.createCheese().getClass());
        assertEquals(bunCheese.toString(),
                depokPizzaIngredientFactory.createCheese().toString());

    }

    @Test
    public void testCreateSauce() {

        KetchupSauce ketchupSauce = new KetchupSauce();
        assertEquals(ketchupSauce.getClass(),
                depokPizzaIngredientFactory.createSauce().getClass());
        assertEquals(ketchupSauce.toString(),
                depokPizzaIngredientFactory.createSauce().toString());

    }

    @Test
    public void testCreateClam() {

        OldSkulClams oldSkulClams = new OldSkulClams();
        assertEquals(oldSkulClams.getClass(), depokPizzaIngredientFactory.createClam().getClass());
        assertEquals(oldSkulClams.toString(), depokPizzaIngredientFactory.createClam().toString());
    }

    @Test
    public void testCreateVeggie() {

        Veggies[] veggies = {new CassavaLeaf(), new Eggplant(), new Spinach()};
        Veggies[] nyPizzaVeggies = depokPizzaIngredientFactory.createVeggies();
        IntStream.range(0, veggies.length).forEach(i -> assertEquals(veggies[i].toString(),
                nyPizzaVeggies[i].toString()));

    }
}
