package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class CheeseTest {
    private MozzarellaCheese mozzarellaCheese;
    private ParmesanCheese parmesanCheese;

    @Before
    public void setUp() {
        mozzarellaCheese = new MozzarellaCheese();
        parmesanCheese = new ParmesanCheese();
    }

    @Test
    public void testVeggiesName() {

        assertEquals(mozzarellaCheese.toString(), "Shredded Mozarella");
        assertEquals(parmesanCheese.toString(), "Shredded Parmesan");

    }



}
