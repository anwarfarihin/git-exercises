package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VeggiesTest {
    private BlackOlives blackOlives;
    private Eggplant eggplant;
    private Garlic garlic;
    private Mushroom mushroom;
    private Onion onion;
    private RedPepper redPepper;
    private Spinach spinach;
    private CassavaLeaf cassavaLeaf;

    @Before
    public void setUp() {
        cassavaLeaf = new CassavaLeaf();
        blackOlives = new BlackOlives();
        eggplant = new Eggplant();
        garlic = new Garlic();
        mushroom = new Mushroom();
        onion = new Onion();
        redPepper = new RedPepper();
        spinach = new Spinach();
    }

    @Test
    public void testVeggiesName() {

        assertEquals(blackOlives.toString(), "Black Olives");
        assertEquals(eggplant.toString(), "Eggplant");
        assertEquals(garlic.toString(), "Garlic");
        assertEquals(mushroom.toString(), "Mushrooms");
        assertEquals(onion.toString(), "Onion");
        assertEquals(redPepper.toString(), "Red Pepper");
        assertEquals(spinach.toString(), "Spinach");
        assertEquals(cassavaLeaf.toString(), "Cassava Leaf");

    }



}
